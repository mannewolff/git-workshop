package org.mwolff.pizza;

public class Paprica extends Topics{

    public Paprica(Pizza pizza) {
        super(pizza);
    }

    @Override
    public double getPrice() {
        return base.getPrice() + 2.0;
    }

}
