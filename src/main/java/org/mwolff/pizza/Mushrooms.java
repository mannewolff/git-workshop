package org.mwolff.pizza;

public class Mushrooms extends Topics{

    public Mushrooms(Pizza pizza) {
        super(pizza);
    }

    @Override
    public double getPrice() {
        return base.getPrice() + 1.0;
    }
}
