package org.mwolff.pizza;

public class Topics implements Pizza {

    protected Pizza base;
    public Topics(Pizza pizza) {
        base = pizza;
    }
    public double getPrice() {
        return base.getPrice();
    }
}
