package org.mwolff.pizza;

public interface Pizza {
    double getPrice();
}
