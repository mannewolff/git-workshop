package org.mwolff.varianz;

public class Kind extends Person {
    boolean tapfer = false;
    public Kind(final String n) {super(n); }

    public void stillHalten() {
        if(tapfer)
            System.out.println(name + " hält still");
        else
            System.out.println(name + " sagt AUA und wehrt sich");
    }
    public void tapferSein() {
        tapfer = true;
        System.out.println(name + " ist tapfer");
    }
}
