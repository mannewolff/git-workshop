package org.mwolff.varianz;

public class Person {

    protected String name;
    public String getName() { return name; }

    public Person(final String n) { name = n; }

    public void stillHalten() {
        System.out.println(name + " hält still");
    }
}
