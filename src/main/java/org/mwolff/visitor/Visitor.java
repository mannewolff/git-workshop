package org.mwolff.visitor;

public interface Visitor {
    void visit(Book book);
    void visit(Product product);
}
