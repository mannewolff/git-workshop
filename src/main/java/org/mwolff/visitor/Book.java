package org.mwolff.visitor;

public class Book extends Product implements Visitable {

    private double weight;

    public double getWeight() {
        return weight;
    }

    public Book(final double price, final double weight) {
        super(price);
        this.price = price;
        this.weight = weight;
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
