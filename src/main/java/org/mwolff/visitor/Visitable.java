package org.mwolff.visitor;

/**
 * An element which can be visited.
 */
public interface Visitable {

    void accept(Visitor visitor);
}
