package org.mwolff.visitor;

public class ProductVisitor implements Visitor {

    private double totalPriceForCart;
    private double totalWeightForCart;

    public double getTotalPriceForCart() {
        return totalPriceForCart;
    }
    public double getTotalWeightForCart() {return totalWeightForCart;}

    @Override
    public void visit(Product product) {
        totalPriceForCart += product.getPrice();
    }

    @Override
    public void visit(Book book) {
        visit((Product) book);
        totalWeightForCart += book.getWeight();
    }
}
