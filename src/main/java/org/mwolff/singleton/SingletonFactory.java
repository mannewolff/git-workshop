package org.mwolff.singleton;

public class SingletonFactory {

    private static final Singleton singleton = new Singleton();

    public static Singleton getSingletonInstance() {
        return singleton;
    }
}


/*


    public Singleton getInstance() {
        if (singleton == null) {
            synchronized (this) {
                if (singleton == null) {
                    singleton = new Singleton();
                }
            }
        }
        return singleton;
    }
}
*/