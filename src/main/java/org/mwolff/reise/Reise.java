package org.mwolff.reise;

import java.util.Date;

public abstract class Reise {

    protected double grundPreis;
    private Date reiseBeginn;

    public Reise(Date reiseBeginn, double grundPreis) {
        this.reiseBeginn = reiseBeginn;
        this.grundPreis = grundPreis;
    }

    public int tageBestimmen() {
        return 0;
    }

    public abstract double preisBerechnen();

}
