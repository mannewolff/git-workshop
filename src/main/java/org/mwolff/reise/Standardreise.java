package org.mwolff.reise;

import java.util.Date;

public class Standardreise extends Reise {


    public Standardreise(Date reiseBeginn, double grundPreis) {
        super(reiseBeginn, grundPreis);
    }

    @Override
    public double preisBerechnen() {
        return 0;
    }
}
