package org.mwolff.annotation;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.annotation.Annotation;
import java.lang.reflect.Modifier;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * http://tutorials.jenkov.com/java-reflection/annotations.html
 */
public class ReflectionTest {


    Class aClass;

    @BeforeEach
    void setup() throws ClassNotFoundException {
        aClass = Class.forName("org.mwolff.annotation.AnnotatedClass");
    }

    @Test
    void readTypeAnnotation() {
        Annotation[] annotations = aClass.getAnnotations();
        for (Annotation annotation : annotations) {
            if (annotation instanceof MyAnnotation) {
                MyAnnotation myAnnotation = (MyAnnotation) annotation;
                assertThat(myAnnotation.name()).isEqualTo("myName");
                assertThat(myAnnotation.value()).isEqualTo("Hello World");
            }
        }
    }

    /**
     PUBLIC
     PROTECTED
     PRIVATE
     ABSTRACT
     STATIC
     FINAL
     TRANSIENT
     VOLATILE
     SYNCHRONIZED
     NATIVE
     STRICT
     INTERFACE
     */
    @Test
    void readClassModifiers() {
        int modifiers = aClass.getModifiers();
        assertThat(checkModifier(modifiers, Modifier.FINAL)).isTrue();
        assertThat(checkModifier(modifiers, Modifier.PUBLIC)).isTrue();
        assertThat(checkModifier(modifiers, Modifier.PRIVATE)).isFalse();
        assertThat(checkModifier(modifiers, Modifier.ABSTRACT)).isFalse();
    }

    private boolean checkModifier(int modifiers, int Mask) {
        return (modifiers & Mask) == Mask;
    }

}
