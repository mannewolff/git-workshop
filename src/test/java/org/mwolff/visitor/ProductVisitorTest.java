package org.mwolff.visitor;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.*;

class ProductVisitorTest {

    @Test
    void testBookVisitorForPrices() {

        List<Visitable> product = new ArrayList<>();
        product.add(new Product(10.0));
        product.add(new Product(20.0));
        product.add(new Book(30.0, 500.0));

        var visitor = new ProductVisitor();
        product.forEach(item -> item.accept(visitor));

        assertThat(visitor.getTotalPriceForCart()).isEqualTo(60.0);
        assertThat(visitor.getTotalWeightForCart()).isEqualTo(500.0);

    }
}