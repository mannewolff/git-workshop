package org.mwolff.command;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mwolff.command.parameterobject.DefaultParameterObject;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mwolff.command.CommandTransition.SUCCESS;

@DisplayName("some tests on commands")
public class CommandTest {

    @Test
    @DisplayName("starting command as lamda")
    void commandLambdaTest() {

        var myContext = new DefaultParameterObject();

        var container = new DefaultCommandContainer<DefaultParameterObject>().addCommand(
                (context) -> {
                    context.put("Hallo", "Welt");
                    return SUCCESS;
                });

        assertThat(container.executeCommand(myContext)).isEqualTo(SUCCESS);
        assertThat(myContext.get("Hallo")).isEqualTo("Welt");
    }

    @Test
    @DisplayName("order test for command container")
    void commandOrderTest() {

        Command<DefaultParameterObject> command1 = (context) -> {
            var order = context.getAsString("order");
            context.put("order", order + " -1");
            return SUCCESS;
        };

        Command<DefaultParameterObject> command2 = (context) -> {
            var order = context.getAsString("order");
            context.put("order", order + " -2");
            return SUCCESS;
        };

        var context = new DefaultParameterObject();
        var container = new DefaultCommandContainer<DefaultParameterObject>()
                .addCommand(2, command2)
                .addCommand(1, command1)
                .executeCommand(context);

        assertThat(context.getAsString("order")).isEqualTo(" -1 -2");

    }
}
