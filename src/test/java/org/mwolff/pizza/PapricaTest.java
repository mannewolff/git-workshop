package org.mwolff.pizza;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class PapricaTest {

    @Test
    @DisplayName("Die Magarita mit Pilzen und Paprika kostet 13.0")
    void getPriceTest() {
        Pizza myPizza = new Paprica(new Mushrooms(new Magarita()));
        double price = myPizza.getPrice();
        assertThat(price).isEqualTo(13.0);
    }

}