package org.mwolff.pizza;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.assertj.core.api.Assertions.assertThat;

class MagaritaTest {

    @Test
    @DisplayName("Die Magarita kostet 10.0")
    void getPriceTest() {
        Pizza magarita = new Magarita();
        double price = magarita.getPrice();
        assertThat(price).isEqualTo(10.0);
    }

}