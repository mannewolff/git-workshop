package org.mwolff.pizza;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class MushroomsTest {

    @Test
    @DisplayName("Die Magarita mit Pilzen kostet 11.0")
    void getPriceTest() {
        Topics topics = new Mushrooms(new Magarita());
        double price = topics.getPrice();
        assertThat(price).isEqualTo(11.0);
    }

}