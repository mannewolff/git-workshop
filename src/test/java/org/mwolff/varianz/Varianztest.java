package org.mwolff.varianz;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mwolff.command.Command;
import static org.assertj.core.api.Assertions.assertThat;

public class Varianztest {

    @DisplayName("Arzt untersucht eine Person")
    @Test
    public void arztUntersuchtPerson() {
        Arzt arzt = new Arzt("Dr. Meier");
        Person person = new Person("Frau Müller");
        arzt.untersuche(person);
    }

    @DisplayName("Arzt als Kinderarzt untersuch eine Person")
    @Test
    // Liskov
    public void arztalsKinderaztUntersuchtPerson() {
        Arzt arzt = new Kinderarzt("Dr. Meier");
        Person person = new Person("Frau Müller");
        arzt.untersuche(person);
    }

    @DisplayName("Kinderarzt sucht eine Person")
    @Test
    public void kinderarztarztUntersuchtPerson() {
        Kinderarzt kinderarzt = new Kinderarzt("Dr. Schulze");
        Person person = new Person("Frau Müller");
        kinderarzt.untersuche(person);
    }

    @DisplayName("Kinderarzt untersucht Kind")
    @Test
    public void KinderarztarztUntersuchtKind() {
        Kinderarzt arzt = new Kinderarzt("Dr. Schulze");
        Kind kind = new Kind("kleine Susi");
        arzt.untersuche(kind);
    }

    @DisplayName("Arzt als Kinderarzt unterscuht Kind")
    @Test
    public void arztUntersuchtKind() {
        Arzt arzt = new Kinderarzt("Dr. Meier");
        Kind kind = new Kind("kleine Susi");
        arzt.untersuche(kind);
    }

}
