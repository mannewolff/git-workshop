package org.mwolff.lamda;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertSame;

/*
Beispiel für die Verwendung eines Comparators. Die Functional Interfaces sind:
int Comparator(o1, o2)
void Consumer(T)
 */
@DisplayName("testing comparator")
public class ComparatorTest {

    private List<String> stringlist;
    private List<String> sortList;

    @BeforeEach
    void setUp() {
        stringlist = Arrays.<String>asList("Fahrrad", "Ein", "Alter", "Mann", "Faehrt");
        sortList = Arrays.<String>asList("Ein", "Mann", "Alter", "Faehrt", "Fahrrad");
    }

    @Test
    public void testSomething() {
        Integer str1 = 12;
        Integer str2 = new Integer(12);

        assertSame(str1, str2);

    }

    @Test
    @DisplayName("old way to use a comparator")
    void comparatorOld() {
        var lengthComparator = new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return Integer.compare(o1.length(), o2.length());
            }
        };
        Collections.sort(stringlist, lengthComparator);
        assertThat(stringlist).isEqualTo(sortList);
    }

    @Test
    @DisplayName("new way to use a comparator")
    void sortAndIterate() throws Exception {
        final List<String> names = Arrays.<String>asList("Fahrrad", "Ein", "Alter", "Mann", "Faehrt");
        // int Comparator(o1, o2)
        names.sort((str1, str2) -> Integer.compare(str1.length(), str2.length()));
        // int Consumer(T)
        names.forEach(name -> System.out.println(name));
        assertThat(names).isEqualTo(sortList);
    }

}
